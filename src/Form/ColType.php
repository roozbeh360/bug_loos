<?php

namespace App\Form;

/**
 * col form to save or edit col entity
 */


use App\Entity\Col;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ColType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('type',ChoiceType::class,[
                'choices'=> [
                    Col::TYPE_BOOLEAN => col::TYPE_BOOLEAN,
                    col::TYPE_DATE =>  col::TYPE_DATE,
                    col::TYPE_FLOAT => col::TYPE_FLOAT,
                    col::TYPE_STRING => col::TYPE_STRING
                ]
            ])
            ->add('colLength')
            ->add('searchAble')
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Col::class,
        ]);
    }
}
