<?php

namespace App\Entity;

/**
 * this entity represent the cols that need to be define for list grid view
 */


use App\Repository\ColRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ColRepository::class)
 */
class Col
{
    const TYPE_STRING = 'STRING';
    const TYPE_INTEGER = 'INTEGER';
    const TYPE_FLOAT = 'FLOAT';
    const TYPE_DATE = 'DATE';
    const TYPE_BOOLEAN = 'BOOLEAN';


    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $colLength;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $searchAble;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getColLength(): ?string
    {
        return $this->colLength;
    }

    /**
     * @param string|null $colLength
     * @return $this
     */
    public function setColLength(?string $colLength): self
    {
        $this->colLength = $colLength;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getSearchAble(): ?bool
    {
        return $this->searchAble;
    }

    /**
     * @param bool|null $searchAble
     * @return $this
     */
    public function setSearchAble(?bool $searchAble): self
    {
        $this->searchAble = $searchAble;

        return $this;
    }

}
