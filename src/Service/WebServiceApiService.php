<?php

namespace App\Service;

use App\Interfaces\DataSourceAdapterInterface;


/**
 * Class WebServiceApiService
 * @package App\Service
 *
 * symfony service for using of custom web service with data source standards
 */
class WebServiceApiService implements DataSourceAdapterInterface
{

    public function fetchData()
    {
        // TODO: Implement fetchData() method.
    }


    /**
     * @param string $name
     * @param $value
     * @return array
     */
    public function searchByColName(string $name, $value): array
    {
        // TODO: Implement searchByColName() method.
    }
}