<?php
namespace App\Service;

use App\Interfaces\DataSourceAdapterInterface;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class DatabaseService
 * @package App\Service
 *
 * symfony service for using entity manager with data source standards
 */
class DatabaseService implements DataSourceAdapterInterface
{

    private $entityManager ;

    public function __construct(EntityManagerInterface $em)
    {
        $this->entityManager = $em ;
    }


    public function fetchData()
    {
        return [];

    }


    /**
     * @param string $name
     * @param $value
     * @return array
     */
    public function searchByColName(string $name, $value): array
    {
        // TODO: Implement searchByColName() method.
    }
}