<?php


namespace App\Interfaces;

/**
 * Interface DataSourceAdapterInterface
 * @package App\Interfaces
 * use this interface adapter for implementing data sources fetch standards
 */
interface DataSourceAdapterInterface
{
    /**
     * @return mixed
     */
    public function fetchData();

    public function searchByColName(string $name,$value) : array ;
}