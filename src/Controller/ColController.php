<?php

namespace App\Controller;

use App\Entity\Col;
use App\Form\ColType;
use App\Repository\ColRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\DatabaseService;

#[Route('/col')]
class ColController extends AbstractController
{
    // cols lists
    #[Route('/', name: 'col_index', methods: ['GET'])]
    public function index(ColRepository $colRepository): Response
    {
        return $this->render('col/index.html.twig', [
            'cols' => $colRepository->findAll(),
        ]);
    }

    // create cols with definitions
    #[Route('/new', name: 'col_new', methods: ['GET', 'POST'])]
    public function new(Request $request): Response
    {
        $col = new Col();
        $form = $this->createForm(ColType::class, $col);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($col);
            $entityManager->flush();

            return $this->redirectToRoute('col_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('col/new.html.twig', [
            'col' => $col,
            'form' => $form,
        ]);
    }

    // col show the setting
    #[Route('/{id}', name: 'col_show', methods: ['GET'])]
    public function show(Col $col): Response
    {
        return $this->render('col/show.html.twig', [
            'col' => $col,
        ]);
    }

    // col edit
    #[Route('/{id}/edit', name: 'col_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Col $col): Response
    {
        $form = $this->createForm(ColType::class, $col);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('col_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('col/edit.html.twig', [
            'col' => $col,
            'form' => $form,
        ]);
    }

    // col remove from list
    #[Route('/{id}', name: 'col_delete', methods: ['POST'])]
    public function delete(Request $request, Col $col): Response
    {
        if ($this->isCsrfTokenValid('delete'.$col->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($col);
            $entityManager->flush();
        }

        return $this->redirectToRoute('col_index', [], Response::HTTP_SEE_OTHER);
    }


}
